import { File } from './file/file.entity';
import { Recipe } from './recipe/recipe.entity';
import { User } from './user/user.entity';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule} from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { RecipeModule } from './recipe/recipe.module';
import { FileModule } from './file/file.module';

@Module({
  imports: [
    UserModule,
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: 'mongodb://localhost:27017/vidly',
      synchronize: true,
      useUnifiedTopology: true,
      entities: [  User, Recipe, File ]
    }),
    GraphQLModule.forRoot({
      autoSchemaFile: true,
      uploads: {
        maxFileSize: 20000000, // 20 MB
        maxFiles: 5
      }
    }),
    RecipeModule,
    FileModule,],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
