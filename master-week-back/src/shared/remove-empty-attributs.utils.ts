
export function removeEmptyAttribut(obj: any
): any {
  Object.keys(obj).forEach(key => obj[key] == null && delete obj[key]);
  return obj;
}

export function arrayRemove(arr, value) { 
    
  return arr.filter(function(ele){ 
      return ele != value; 
  });
}