import { removeEmptyAttribut, arrayRemove } from 'src/shared/remove-empty-attributs.utils';
import { User } from './user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { v4 as uuid } from 'uuid';
import { CreateUserInput } from './user.input';
import * as jwt from 'jsonwebtoken';
import { UpdateUser } from './user.model';

@Injectable()
export class UserService {
    constructor(
    @InjectRepository(User) private userRepository: Repository<User>
    )
    {}

    createUser(createUserInput: CreateUserInput) {
        const { firstName, familyName, email, password, image} = createUserInput;
        const user =  this.userRepository.create({
            id: uuid(),
            firstName,
            familyName,
            email,
            password,
            recipes: [],
            image
        });

        return this.userRepository.save(user); 
    }

    async update(email: string, updateUserInput: UpdateUser) {
        const newCreateUserInput = removeEmptyAttribut(updateUserInput);
        const property =  await this.userRepository.findOne({email})
        return this.userRepository.save({
            ...property,
            ...newCreateUserInput
            })
    }

    async delete(id: string) {
        return await this.userRepository.delete({id})
        .then(() => {return true; }).catch(() => {return false});

    }

    getUser(id): Promise<User> {
        return this.userRepository.findOne({id});
    }

    getAllUsers(): Promise<User[]> {
        return this.userRepository.find();
    }

    getUserByEmail(email: string) {
        return this.userRepository.findOne({ email });
    }

    createToken({ id, email }: User) {
        return jwt.sign({ id, email }, 'secret');
      }
    
      
    // Recipes
    async addRecipeToUser(email: string, idRecipe: string) {
        const property =  await this.userRepository.findOne({ email });
        console.log(property);
        return this.userRepository.save({
            ...property, // existing fields
             recipes: [...property.recipes, idRecipe]
          });
    }

    async removeRecipeFromUser(email: string, idRecipe: string) {
        const property =  await this.userRepository.findOne({ email });
        return this.userRepository.save({
            ...property,
             recipes: [...arrayRemove(property.recipes, idRecipe)]
          });
    }


      // Search

      searchUserByFirstName(firstName: string): Promise<User[]> {
        return this.userRepository.find({
            where : {
                firstName : new RegExp(`^${firstName}`)
              }
       });
   }

   searchUserByFamilyName(FamilyName: string): Promise<User[]> {
        return this.userRepository.find({
            where : {
                familyName : new RegExp(`^${FamilyName}`)
              }
       });
   }
}