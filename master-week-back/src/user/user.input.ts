import { InputType, Field } from "@nestjs/graphql";
import { MinLength, IsDateString, IsEmail } from "class-validator";

@InputType()
export class CreateUserInput {

    @MinLength(1)
    @Field()
    firstName: string;

    @MinLength(1)
    @Field()
    familyName: string;

    @IsEmail()
    @Field()
    email: string;

    @Field()
    password: string;

    @Field()
    recipes: string[];

    @Field()
    image: string;

}