import { ObjectType, Field, ID } from "@nestjs/graphql";

@ObjectType('User')
export class UserType {
    @Field(type => ID)
    id: string;

    @Field()
    firstName: string;

    @Field()
    familyName: string;
    
    @Field()
    email: string;

    @Field()
    password: string;

    @Field(type => [String])
    recipes: string[];

    @Field()
    image: string;

}