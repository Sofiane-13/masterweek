import { Entity, PrimaryColumn, Column, ObjectIdColumn } from "typeorm";

@Entity()
export class User {
    @ObjectIdColumn()
    _id: string;
    
    @PrimaryColumn()
    id: string;

    @Column()
    firstName: string;

    @Column()
    familyName: string;

    @Column()
    email: string;

    @Column()
    password: string;  
    
    @Column()
    recipes: string[];  

    @Column()
    image: string;  
}