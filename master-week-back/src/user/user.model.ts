export interface UpdateUser {

    firstName: string;
    familyName: string;
    email: string;
    password: string;
    image: string;
  }