import { UserService } from './user.service'
import { UserType } from './user.type'
import { Resolver, Query, Mutation, Args, Context } from "@nestjs/graphql"
import { AuthGuard } from './auth.gaurd'
import { UseGuards } from '@nestjs/common'
import { User } from './user.entity'
import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { HttpStatus } from '@nestjs/common';
@Resolver(of => UserType)
export class UserResolver {
    constructor(
        private userService: UserService
    ) {

    }

    @Query(returns => UserType)
    user(
        @Args('id') id: string,
    ) {
        return this.userService.getUser(id) 
    }

    @Query(returns => [UserType])
    users() {
        return this.userService.getAllUsers() 
    }

    @Query(returns => UserType)
    @UseGuards(new AuthGuard())
    async me(@Context('user') user: User) {
      return await this.userService.getUserByEmail(user.email)
    }

    // search
    @Query(returns => [UserType])
    searchUserByFirstName(
        @Args('term') term: string) {
        return this.userService.searchUserByFirstName(term) 
    }

    @Query(returns => [UserType])
    searchUserByFamilyName(
        @Args('term') term: string) {
        return this.userService.searchUserByFamilyName(term) 
    }

    
    @Mutation(returns => UserType)
    createUser(
        @Args('firstName') firstName: string,
        @Args('familyName') familyName: string,
        @Args('email') email: string,
        @Args('password') password: string,
        @Args('image') image: string

    ) {
        return this.userService.createUser(
            {
            firstName,
            familyName,
            email,
            password,
            recipes: [],
            image}
        )
    }

    @UseGuards(new AuthGuard())
    @Mutation(returns => UserType)
    updateUser(
        @Context('user') userToken: User,      
        @Args({
            name: 'firstName',
            type: () => String,
            nullable: true
          }) firstName: string,
          @Args({
            name: 'familyName',
            type: () => String,
            nullable: true
          }) familyName: string,
          @Args({
            name: 'email',
            type: () => String,
            nullable: true
          }) email: string,
          @Args({
            name: 'password',
            type: () => String,
            nullable: true
          }) password: string,
          @Args({
            name: 'image',
            type: () => String,
            nullable: true
          }) image: string
    ) {
        return this.userService.update(userToken.email, {
            firstName,
            familyName,
            email,
            password,
            image}) 
    }

    @Mutation(returns => Boolean)
    deleteUser(
        @Args('id') id: string,
    ) {
        return this.userService.delete(id) 
    }

    @Mutation(returns => String)
    async login(@Args('email') email: string, @Args('password') password: string) {
      let user = await this.userService.getUserByEmail(email)
      if (!user) {
        const errors = {email: 'email not found.'};
        throw new HttpException({message: 'Login failed', errors}, HttpStatus.NOT_FOUND);
      } else if(user.password === password){
        return this.userService.createToken(user)
      }
    }


    // Recipes
    @UseGuards(new AuthGuard())
    @Mutation(returns => UserType)
    async addRecipeToUser(@Context('user') userToken: User, @Args('idRecipe') idRecipe: string) {
      
      return this.userService.addRecipeToUser(userToken.email, idRecipe)
    }

    @UseGuards(new AuthGuard())
    @Mutation(returns => UserType)
    async removeRecipeFromUser(@Context('user') userToken: User, @Args('idRecipe') idRecipe: string) {
        
        return this.userService.removeRecipeFromUser(userToken.email, idRecipe)
    }
}