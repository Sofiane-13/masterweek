import { FileService } from './file.service'
import { Resolver, Query, Mutation, Args, Context, Float } from "@nestjs/graphql"
import { GraphQLUpload } from "apollo-server-express";
import { FileType } from './file.type';

@Resolver(of => FileType)
export class FileResolver {
    constructor(
        private fileService:  FileService
    ) {

    }


    @Query(returns => String)
    file(
        @Args('id') id: string,
    ) {
        return this.fileService.getFile(id);
    }

    @Mutation(() => String)
    async uploadFile(
     @Args({ name: 'file', type: (): typeof GraphQLUpload => GraphQLUpload })
     file: Buffer,
    ) {
        const result = await this.fileService.createFile(file);
        return result.id;
    }
  }


