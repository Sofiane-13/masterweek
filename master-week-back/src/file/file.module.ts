import { File } from './file.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { FileResolver } from './file.resolver';
import { FileService } from './file.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([File])
    ],
    providers: [FileResolver, FileService]
})
export class FileModule {

}
