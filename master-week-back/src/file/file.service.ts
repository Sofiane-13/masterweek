import { File } from './file.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { v4 as uuid } from 'uuid';

@Injectable()
export class FileService {
    constructor(
    @InjectRepository(File) private fileRepository: Repository<File>
    )
    {
    }

    async getFile(id: string): Promise<any> {
        const image = await this.fileRepository.findOne({id});
        return image.image
    }


    async createFile(createFileInput: any) {
        const { createReadStream,  filename} = createFileInput;
        const picture = await this.gettingStream(createReadStream, filename);
        const recipe =  this.fileRepository.create({
            id: uuid(),
            image: picture.toString('base64')
        });

        return this.fileRepository.save(recipe); 
    }

    async gettingStream(
        createReadStream: Function,
        filename: string,
      ): Promise<Buffer> {
        let buffer = [];
        return new Promise(resolve =>
          createReadStream()
            .on('data', data => buffer.push(data))
            .on('end', () => resolve(Buffer.concat(buffer))),
        );
      }
}