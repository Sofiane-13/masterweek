import { ObjectType, Field, ID } from "@nestjs/graphql";

@ObjectType('File')
export class FileType {
    @Field(type => ID)
    id: string;

    @Field()
    image: Buffer;
}