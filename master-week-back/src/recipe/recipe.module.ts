import { RecipeService } from './recipe.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { Recipe } from './recipe.entity';
import { RecipeResolver } from './recipe.resolver';

@Module({
    imports: [
        TypeOrmModule.forFeature([Recipe])
    ],
    providers: [RecipeResolver, RecipeService]
})
export class RecipeModule {}
