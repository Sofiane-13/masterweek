import { InputType, Field } from "@nestjs/graphql";
import { IsDateString } from "class-validator";

@InputType()
export class CreateRecipeInput {

    @Field()
    images: string[];

    @Field()
    title: string;

    @Field()
    description: string;

    @IsDateString()
    @Field()
    dateCreation: Date;

    @Field()
    usersId: string;

}