import { Recipe } from './recipe.entity'
import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository, MoreThan } from 'typeorm'
import { v4 as uuid } from 'uuid'
import { CreateRecipeInput } from './recipe.input'
import { removeEmptyAttribut, arrayRemove } from 'src/shared/remove-empty-attributs.utils'
import { UpdateRecipe } from './recipe.model'
import {Like} from "typeorm";
@Injectable()
export class RecipeService {
    constructor(
    @InjectRepository(Recipe) private recipeRepository: Repository<Recipe>
    )
    {}

    createRecipe(createRecipeInput: CreateRecipeInput) {
        const { images, title, description, dateCreation, usersId} = createRecipeInput
        const recipe =  this.recipeRepository.create({
            id: uuid(),
            images,
            title,
            description,
            dateCreation,
            usersIdsLiked: [],
            likes: 0,
            usersId
        })

        return this.recipeRepository.save(recipe) 
    }

    async delete(id: string) {
        return await this.recipeRepository.delete({id})
        .then(() => {return true }).catch(() => {return false})
    }

    async update(id: string, updateRecipeInput: UpdateRecipe) {
        const newCreateRecipeInput = removeEmptyAttribut(updateRecipeInput)
        const property =  await this.recipeRepository.findOne({id})
        return this.recipeRepository.save({
            ...property,
            ...newCreateRecipeInput
            })
    }

    getRecipe(id: string): Promise<Recipe> {
        return this.recipeRepository.findOne({id})
    }

    searchRecipesByTitle(title: string): Promise<Recipe[]> {
         return this.recipeRepository.find({
            where: {
                title: new RegExp(`^${title}`)
            }
        });
    }

    getAllRecipes(): Promise<Recipe[]> {
        return this.recipeRepository.find()
    }

    getAllRecipesOfTheWeek(skip: number, take: number): Promise<Recipe[]> {
        return this.recipeRepository.find({
            order: {
                dateCreation: "DESC"
            },
            skip,
            take
        });
    }

    // Likes
    async addLikeToRecipe(id: string, idRecipe: string) {
        const property =  await this.recipeRepository.findOne({ id: idRecipe })
        return this.recipeRepository.save({
            ...property,
            usersIdsLiked: [...property.usersIdsLiked, id],
            likes: property.likes + 1
            })
    }

    async removeLikeFromRecipe(id: string, idRecipe: string) {
        const property =  await this.recipeRepository.findOne({ id: idRecipe })

        return this.recipeRepository.save({
            ...property,
            usersIdsLiked: [...arrayRemove(property.usersIdsLiked, id)],
            likes: property.likes - 1
            })
    }
}