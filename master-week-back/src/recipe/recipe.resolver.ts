import { User } from './../user/user.entity'
import { RecipeService } from './recipe.service'
import { RecipeType } from './recipe.type'
import { Resolver, Query, Mutation, Args, Context } from "@nestjs/graphql"
import { UseGuards } from '@nestjs/common'
import { AuthGuard } from 'src/user/auth.gaurd'



@Resolver(of => RecipeType)
export class RecipeResolver {
    constructor(
        private recipeService:  RecipeService
    ) {

    }

    @Query(returns => RecipeType)
    recipe(
        @Args('id') id: string,
    ) {
        return this.recipeService.getRecipe(id) 
    }

    @Query(returns => [RecipeType])
    recipes() {
        return this.recipeService.getAllRecipes() 
    }

    @Query(returns => [RecipeType])
    searchRecipesByTitle(
        @Args('title') title: string,
    ) {
        return this.recipeService.searchRecipesByTitle(title) 
    }

    @Query(returns => [RecipeType])
    recipesFoTheWeek(
        @Args('skip') skip: number,
        @Args('take') take: number,
    ) {
        return this.recipeService.getAllRecipesOfTheWeek(skip, take) 
    }

    @Mutation(returns => RecipeType)
    createRecipe(
        @Args({
            name: 'images',
            type: () => [String]
          }) images: string[],
        @Args('title') title: string,
        @Args('description') description: string,
        @Args('dateCreation') dateCreation: Date,
        @Args('usersId') usersId: string,


    ) {
        return this.recipeService.createRecipe(
            {
            images,
            title,
            description,
            dateCreation,
            usersId}
        )
    }

    @Mutation(returns => Boolean)
    deleteRecipe(
        @Args('id') id: string,
    ) {
        return this.recipeService.delete(id) 
    }


    @Mutation(returns => RecipeType)
    updateRecipe(
        @Args('id') id: string,       
        @Args({
            name: 'images',
            type: () => [String],
            nullable: true
          }) images: string[],
          @Args({
            name: 'title',
            type: () => String,
            nullable: true
          }) title: string,
          @Args({
            name: 'description',
            type: () => String,
            nullable: true
          }) description: string
    ) {
        return this.recipeService.update(id, {
            images,
            title,
            description}) 
    }


    @UseGuards(new AuthGuard())
    @Mutation(returns => RecipeType)
    async addLikeToRecipe(@Context('user') userToken: User, @Args('idRecipe') idRecipe: string) {
      
      return this.recipeService.addLikeToRecipe(userToken.id, idRecipe)
    }

    @UseGuards(new AuthGuard())
    @Mutation(returns => RecipeType)
    async removeLikeFromRecipe(@Context('user') userToken: User, @Args('idRecipe') idRecipe: string) {
      
      return this.recipeService.removeLikeFromRecipe(userToken.id, idRecipe)
    }

  }
