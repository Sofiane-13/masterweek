import { ObjectType, Field, ID } from "@nestjs/graphql";

@ObjectType('Recipe')
export class RecipeType {
    @Field(type => ID)
    id: string;

    @Field(type => [String])
    images: string[];

    @Field()
    title: string;
    
    @Field()
    description: string;

    @Field()
    dateCreation: Date;

    @Field(type => [String])
    usersIdsLiked: string[];

    @Field(type => Number)
    likes: number;

    @Field(type => String)
    usersId: string;

}