export interface UpdateRecipe {
    images?: string[];
    title?: string;
    description?: string;
  }