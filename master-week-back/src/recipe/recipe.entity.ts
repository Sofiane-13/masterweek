import { Entity, PrimaryColumn, Column, ObjectIdColumn } from "typeorm";

@Entity()
export class Recipe {
    @ObjectIdColumn()
    _id: string;
    
    @PrimaryColumn()
    id: string;

    @Column()
    images: string[];

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    dateCreation: Date;  
    
    @Column()
    usersIdsLiked: string[];  

    @Column()
    likes: number;  

    @Column()
    usersId: string;
}