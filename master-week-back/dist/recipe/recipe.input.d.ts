export declare class CreateRecipeInput {
    images: string[];
    title: string;
    description: string;
    dateCreation: Date;
    usersId: string;
}
