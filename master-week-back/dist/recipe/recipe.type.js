"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecipeType = void 0;
const graphql_1 = require("@nestjs/graphql");
let RecipeType = class RecipeType {
};
__decorate([
    graphql_1.Field(type => graphql_1.ID),
    __metadata("design:type", String)
], RecipeType.prototype, "id", void 0);
__decorate([
    graphql_1.Field(type => [String]),
    __metadata("design:type", Array)
], RecipeType.prototype, "images", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], RecipeType.prototype, "title", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], RecipeType.prototype, "description", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Date)
], RecipeType.prototype, "dateCreation", void 0);
__decorate([
    graphql_1.Field(type => [String]),
    __metadata("design:type", Array)
], RecipeType.prototype, "usersIdsLiked", void 0);
__decorate([
    graphql_1.Field(type => Number),
    __metadata("design:type", Number)
], RecipeType.prototype, "likes", void 0);
__decorate([
    graphql_1.Field(type => String),
    __metadata("design:type", String)
], RecipeType.prototype, "usersId", void 0);
RecipeType = __decorate([
    graphql_1.ObjectType('Recipe')
], RecipeType);
exports.RecipeType = RecipeType;
//# sourceMappingURL=recipe.type.js.map