import { User } from './../user/user.entity';
import { RecipeService } from './recipe.service';
export declare class RecipeResolver {
    private recipeService;
    constructor(recipeService: RecipeService);
    recipe(id: string): Promise<import("./recipe.entity").Recipe>;
    recipes(): Promise<import("./recipe.entity").Recipe[]>;
    createRecipe(image: string, title: string, description: string, dateCreation: Date, usersId: string): Promise<import("./recipe.entity").Recipe>;
    deleteRecipe(id: string): Promise<import("typeorm").DeleteResult>;
    addLikeToRecipe(userToken: User, idRecipe: string): Promise<{
        usersIdsLiked: string[];
        _id: string;
        id: string;
        image: string;
        title: string;
        description: string;
        dateCreation: Date;
        usersId: string;
    } & import("./recipe.entity").Recipe>;
}
