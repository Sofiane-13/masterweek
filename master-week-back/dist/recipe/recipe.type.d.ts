export declare class RecipeType {
    id: string;
    images: string[];
    title: string;
    description: string;
    dateCreation: Date;
    usersIdsLiked: string[];
    likes: number;
    usersId: string;
}
