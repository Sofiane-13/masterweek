export declare class Recipe {
    _id: string;
    id: string;
    images: string[];
    title: string;
    description: string;
    dateCreation: Date;
    usersIdsLiked: string[];
    likes: number;
    usersId: string;
}
