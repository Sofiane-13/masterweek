"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecipeResolver = void 0;
const user_entity_1 = require("./../user/user.entity");
const recipe_service_1 = require("./recipe.service");
const recipe_type_1 = require("./recipe.type");
const graphql_1 = require("@nestjs/graphql");
const common_1 = require("@nestjs/common");
const auth_gaurd_1 = require("../user/auth.gaurd");
let RecipeResolver = class RecipeResolver {
    constructor(recipeService) {
        this.recipeService = recipeService;
    }
    recipe(id) {
        return this.recipeService.getRecipe(id);
    }
    recipes() {
        return this.recipeService.getAllRecipes();
    }
    createRecipe(image, title, description, dateCreation, usersId) {
        return this.recipeService.createRecipe({
            image,
            title,
            description,
            dateCreation,
            usersId
        });
    }
    deleteRecipe(id) {
        return this.recipeService.delete(id);
    }
    async addLikeToRecipe(userToken, idRecipe) {
        return this.recipeService.addLikeToRecipe(userToken.id, idRecipe);
    }
};
__decorate([
    graphql_1.Query(returns => recipe_type_1.RecipeType),
    __param(0, graphql_1.Args('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], RecipeResolver.prototype, "recipe", null);
__decorate([
    graphql_1.Query(returns => [recipe_type_1.RecipeType]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], RecipeResolver.prototype, "recipes", null);
__decorate([
    graphql_1.Mutation(returns => recipe_type_1.RecipeType),
    __param(0, graphql_1.Args('image')),
    __param(1, graphql_1.Args('title')),
    __param(2, graphql_1.Args('description')),
    __param(3, graphql_1.Args('dateCreation')),
    __param(4, graphql_1.Args('usersId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, Date, String]),
    __metadata("design:returntype", void 0)
], RecipeResolver.prototype, "createRecipe", null);
__decorate([
    graphql_1.Mutation(returns => recipe_type_1.RecipeType),
    __param(0, graphql_1.Args('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], RecipeResolver.prototype, "deleteRecipe", null);
__decorate([
    common_1.UseGuards(new auth_gaurd_1.AuthGuard()),
    graphql_1.Mutation(returns => recipe_type_1.RecipeType),
    __param(0, graphql_1.Context('user')), __param(1, graphql_1.Args('idRecipe')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User, String]),
    __metadata("design:returntype", Promise)
], RecipeResolver.prototype, "addLikeToRecipe", null);
RecipeResolver = __decorate([
    graphql_1.Resolver(of => recipe_type_1.RecipeType),
    __metadata("design:paramtypes", [recipe_service_1.RecipeService])
], RecipeResolver);
exports.RecipeResolver = RecipeResolver;
//# sourceMappingURL=reciep.resolver.js.map