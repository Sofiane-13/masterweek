import { Recipe } from './recipe.entity';
import { Repository } from 'typeorm';
import { CreateRecipeInput } from './recipe.input';
import { UpdateRecipe } from './recipe.model';
export declare class RecipeService {
    private recipeRepository;
    constructor(recipeRepository: Repository<Recipe>);
    createRecipe(createRecipeInput: CreateRecipeInput): Promise<Recipe>;
    delete(id: string): Promise<boolean>;
    update(id: string, updateRecipeInput: UpdateRecipe): Promise<any>;
    getRecipe(id: string): Promise<Recipe>;
    searchRecipesByTitle(title: string): Promise<Recipe[]>;
    getAllRecipes(): Promise<Recipe[]>;
    getAllRecipesOfTheWeek(skip: number, take: number): Promise<Recipe[]>;
    addLikeToRecipe(id: string, idRecipe: string): Promise<{
        usersIdsLiked: string[];
        likes: number;
        _id: string;
        id: string;
        images: string[];
        title: string;
        description: string;
        dateCreation: Date;
        usersId: string;
    } & Recipe>;
    removeLikeFromRecipe(id: string, idRecipe: string): Promise<{
        usersIdsLiked: any[];
        likes: number;
        _id: string;
        id: string;
        images: string[];
        title: string;
        description: string;
        dateCreation: Date;
        usersId: string;
    } & Recipe>;
}
