import { User } from './../user/user.entity';
import { RecipeService } from './recipe.service';
export declare class RecipeResolver {
    private recipeService;
    constructor(recipeService: RecipeService);
    recipe(id: string): Promise<import("./recipe.entity").Recipe>;
    recipes(): Promise<import("./recipe.entity").Recipe[]>;
    searchRecipesByTitle(title: string): Promise<import("./recipe.entity").Recipe[]>;
    recipesFoTheWeek(skip: number, take: number): Promise<import("./recipe.entity").Recipe[]>;
    createRecipe(images: string[], title: string, description: string, dateCreation: Date, usersId: string): Promise<import("./recipe.entity").Recipe>;
    deleteRecipe(id: string): Promise<boolean>;
    updateRecipe(id: string, images: string[], title: string, description: string): Promise<any>;
    addLikeToRecipe(userToken: User, idRecipe: string): Promise<{
        usersIdsLiked: string[];
        likes: number;
        _id: string;
        id: string;
        images: string[];
        title: string;
        description: string;
        dateCreation: Date;
        usersId: string;
    } & import("./recipe.entity").Recipe>;
    removeLikeFromRecipe(userToken: User, idRecipe: string): Promise<{
        usersIdsLiked: any[];
        likes: number;
        _id: string;
        id: string;
        images: string[];
        title: string;
        description: string;
        dateCreation: Date;
        usersId: string;
    } & import("./recipe.entity").Recipe>;
}
