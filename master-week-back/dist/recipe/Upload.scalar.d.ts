export declare class UploadFile {
    description: string;
    parseValue(value: any): any;
    serialize(value: any): any;
    parseLiteral(ast: any): any;
}
