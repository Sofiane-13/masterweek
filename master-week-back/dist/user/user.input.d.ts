export declare class CreateUserInput {
    firstName: string;
    familyName: string;
    email: string;
    password: string;
    recipes: string[];
    image: string;
}
