import { User } from './user.entity';
import { Repository } from 'typeorm';
import { CreateUserInput } from './user.input';
import { UpdateUser } from './user.model';
export declare class UserService {
    private userRepository;
    constructor(userRepository: Repository<User>);
    createUser(createUserInput: CreateUserInput): Promise<User>;
    update(email: string, updateUserInput: UpdateUser): Promise<any>;
    delete(id: string): Promise<boolean>;
    getUser(id: any): Promise<User>;
    getAllUsers(): Promise<User[]>;
    getUserByEmail(email: string): Promise<User>;
    createToken({ id, email }: User): string;
    addRecipeToUser(email: string, idRecipe: string): Promise<{
        recipes: string[];
        _id: string;
        id: string;
        firstName: string;
        familyName: string;
        email: string;
        password: string;
        image: string;
    } & User>;
    removeRecipeFromUser(email: string, idRecipe: string): Promise<{
        recipes: any[];
        _id: string;
        id: string;
        firstName: string;
        familyName: string;
        email: string;
        password: string;
        image: string;
    } & User>;
    searchUserByFirstName(firstName: string): Promise<User[]>;
    searchUserByFamilyName(FamilyName: string): Promise<User[]>;
}
