"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserResolver = void 0;
const user_service_1 = require("./user.service");
const user_type_1 = require("./user.type");
const graphql_1 = require("@nestjs/graphql");
const auth_gaurd_1 = require("./auth.gaurd");
const common_1 = require("@nestjs/common");
const user_entity_1 = require("./user.entity");
const http_exception_1 = require("@nestjs/common/exceptions/http.exception");
const common_2 = require("@nestjs/common");
let UserResolver = class UserResolver {
    constructor(userService) {
        this.userService = userService;
    }
    user(id) {
        return this.userService.getUser(id);
    }
    users() {
        return this.userService.getAllUsers();
    }
    async me(user) {
        return await this.userService.getUserByEmail(user.email);
    }
    searchUserByFirstName(term) {
        return this.userService.searchUserByFirstName(term);
    }
    searchUserByFamilyName(term) {
        return this.userService.searchUserByFamilyName(term);
    }
    createUser(firstName, familyName, email, password, image) {
        return this.userService.createUser({
            firstName,
            familyName,
            email,
            password,
            recipes: [],
            image
        });
    }
    updateUser(userToken, firstName, familyName, email, password, image) {
        return this.userService.update(userToken.email, {
            firstName,
            familyName,
            email,
            password,
            image
        });
    }
    deleteUser(id) {
        return this.userService.delete(id);
    }
    async login(email, password) {
        let user = await this.userService.getUserByEmail(email);
        if (!user) {
            const errors = { email: 'email not found.' };
            throw new http_exception_1.HttpException({ message: 'Login failed', errors }, common_2.HttpStatus.NOT_FOUND);
        }
        else if (user.password === password) {
            return this.userService.createToken(user);
        }
    }
    async addRecipeToUser(userToken, idRecipe) {
        return this.userService.addRecipeToUser(userToken.email, idRecipe);
    }
    async removeRecipeFromUser(userToken, idRecipe) {
        return this.userService.removeRecipeFromUser(userToken.email, idRecipe);
    }
};
__decorate([
    graphql_1.Query(returns => user_type_1.UserType),
    __param(0, graphql_1.Args('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "user", null);
__decorate([
    graphql_1.Query(returns => [user_type_1.UserType]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "users", null);
__decorate([
    graphql_1.Query(returns => user_type_1.UserType),
    common_1.UseGuards(new auth_gaurd_1.AuthGuard()),
    __param(0, graphql_1.Context('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "me", null);
__decorate([
    graphql_1.Query(returns => [user_type_1.UserType]),
    __param(0, graphql_1.Args('term')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "searchUserByFirstName", null);
__decorate([
    graphql_1.Query(returns => [user_type_1.UserType]),
    __param(0, graphql_1.Args('term')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "searchUserByFamilyName", null);
__decorate([
    graphql_1.Mutation(returns => user_type_1.UserType),
    __param(0, graphql_1.Args('firstName')),
    __param(1, graphql_1.Args('familyName')),
    __param(2, graphql_1.Args('email')),
    __param(3, graphql_1.Args('password')),
    __param(4, graphql_1.Args('image')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, String]),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "createUser", null);
__decorate([
    common_1.UseGuards(new auth_gaurd_1.AuthGuard()),
    graphql_1.Mutation(returns => user_type_1.UserType),
    __param(0, graphql_1.Context('user')),
    __param(1, graphql_1.Args({
        name: 'firstName',
        type: () => String,
        nullable: true
    })),
    __param(2, graphql_1.Args({
        name: 'familyName',
        type: () => String,
        nullable: true
    })),
    __param(3, graphql_1.Args({
        name: 'email',
        type: () => String,
        nullable: true
    })),
    __param(4, graphql_1.Args({
        name: 'password',
        type: () => String,
        nullable: true
    })),
    __param(5, graphql_1.Args({
        name: 'image',
        type: () => String,
        nullable: true
    })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User, String, String, String, String, String]),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "updateUser", null);
__decorate([
    graphql_1.Mutation(returns => Boolean),
    __param(0, graphql_1.Args('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], UserResolver.prototype, "deleteUser", null);
__decorate([
    graphql_1.Mutation(returns => String),
    __param(0, graphql_1.Args('email')), __param(1, graphql_1.Args('password')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "login", null);
__decorate([
    common_1.UseGuards(new auth_gaurd_1.AuthGuard()),
    graphql_1.Mutation(returns => user_type_1.UserType),
    __param(0, graphql_1.Context('user')), __param(1, graphql_1.Args('idRecipe')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User, String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "addRecipeToUser", null);
__decorate([
    common_1.UseGuards(new auth_gaurd_1.AuthGuard()),
    graphql_1.Mutation(returns => user_type_1.UserType),
    __param(0, graphql_1.Context('user')), __param(1, graphql_1.Args('idRecipe')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User, String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "removeRecipeFromUser", null);
UserResolver = __decorate([
    graphql_1.Resolver(of => user_type_1.UserType),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserResolver);
exports.UserResolver = UserResolver;
//# sourceMappingURL=user.resolver.js.map