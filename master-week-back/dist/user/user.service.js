"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const remove_empty_attributs_utils_1 = require("../shared/remove-empty-attributs.utils");
const user_entity_1 = require("./user.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const uuid_1 = require("uuid");
const jwt = require("jsonwebtoken");
let UserService = class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    createUser(createUserInput) {
        const { firstName, familyName, email, password, image } = createUserInput;
        const user = this.userRepository.create({
            id: uuid_1.v4(),
            firstName,
            familyName,
            email,
            password,
            recipes: [],
            image
        });
        return this.userRepository.save(user);
    }
    async update(email, updateUserInput) {
        const newCreateUserInput = remove_empty_attributs_utils_1.removeEmptyAttribut(updateUserInput);
        const property = await this.userRepository.findOne({ email });
        return this.userRepository.save(Object.assign(Object.assign({}, property), newCreateUserInput));
    }
    async delete(id) {
        return await this.userRepository.delete({ id })
            .then(() => { return true; }).catch(() => { return false; });
    }
    getUser(id) {
        return this.userRepository.findOne({ id });
    }
    getAllUsers() {
        return this.userRepository.find();
    }
    getUserByEmail(email) {
        return this.userRepository.findOne({ email });
    }
    createToken({ id, email }) {
        return jwt.sign({ id, email }, 'secret');
    }
    async addRecipeToUser(email, idRecipe) {
        const property = await this.userRepository.findOne({ email });
        console.log(property);
        return this.userRepository.save(Object.assign(Object.assign({}, property), { recipes: [...property.recipes, idRecipe] }));
    }
    async removeRecipeFromUser(email, idRecipe) {
        const property = await this.userRepository.findOne({ email });
        return this.userRepository.save(Object.assign(Object.assign({}, property), { recipes: [...remove_empty_attributs_utils_1.arrayRemove(property.recipes, idRecipe)] }));
    }
    searchUserByFirstName(firstName) {
        return this.userRepository.find({
            where: {
                firstName: new RegExp(`^${firstName}`)
            }
        });
    }
    searchUserByFamilyName(FamilyName) {
        return this.userRepository.find({
            where: {
                familyName: new RegExp(`^${FamilyName}`)
            }
        });
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map