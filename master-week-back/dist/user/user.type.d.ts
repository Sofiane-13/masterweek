export declare class UserType {
    id: string;
    firstName: string;
    familyName: string;
    email: string;
    password: string;
    recipes: string[];
    image: string;
}
