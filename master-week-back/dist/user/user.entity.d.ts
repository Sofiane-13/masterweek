export declare class User {
    _id: string;
    id: string;
    firstName: string;
    familyName: string;
    email: string;
    password: string;
    recipes: string[];
    image: string;
}
