import { UserService } from './user.service';
import { User } from './user.entity';
export declare class UserResolver {
    private userService;
    constructor(userService: UserService);
    user(id: string): Promise<User>;
    users(): Promise<User[]>;
    me(user: User): Promise<User>;
    searchUserByFirstName(term: string): Promise<User[]>;
    searchUserByFamilyName(term: string): Promise<User[]>;
    createUser(firstName: string, familyName: string, email: string, password: string, image: string): Promise<User>;
    updateUser(userToken: User, firstName: string, familyName: string, email: string, password: string, image: string): Promise<any>;
    deleteUser(id: string): Promise<boolean>;
    login(email: string, password: string): Promise<string>;
    addRecipeToUser(userToken: User, idRecipe: string): Promise<{
        recipes: string[];
        _id: string;
        id: string;
        firstName: string;
        familyName: string;
        email: string;
        password: string;
        image: string;
    } & User>;
    removeRecipeFromUser(userToken: User, idRecipe: string): Promise<{
        recipes: any[];
        _id: string;
        id: string;
        firstName: string;
        familyName: string;
        email: string;
        password: string;
        image: string;
    } & User>;
}
