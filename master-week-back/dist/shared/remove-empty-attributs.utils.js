"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.arrayRemove = exports.removeEmptyAttribut = void 0;
function removeEmptyAttribut(obj) {
    Object.keys(obj).forEach(key => obj[key] == null && delete obj[key]);
    return obj;
}
exports.removeEmptyAttribut = removeEmptyAttribut;
function arrayRemove(arr, value) {
    return arr.filter(function (ele) {
        return ele != value;
    });
}
exports.arrayRemove = arrayRemove;
//# sourceMappingURL=remove-empty-attributs.utils.js.map