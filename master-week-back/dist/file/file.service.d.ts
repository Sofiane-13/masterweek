/// <reference types="node" />
import { File } from './file.entity';
import { Repository } from 'typeorm';
export declare class FileService {
    private fileRepository;
    constructor(fileRepository: Repository<File>);
    getFile(id: string): Promise<any>;
    createFile(createFileInput: any): Promise<File>;
    gettingStream(createReadStream: Function, filename: string): Promise<Buffer>;
}
