/// <reference types="node" />
import { FileService } from './file.service';
export declare class FileResolver {
    private fileService;
    constructor(fileService: FileService);
    file(id: string): Promise<any>;
    uploadFile(file: Buffer): Promise<string>;
}
