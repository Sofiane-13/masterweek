import { Recipe } from './recipe.entity';
import { Repository } from 'typeorm';
import { CreateRecipeInput } from './recipe.input';
export declare class RecipeService {
    private recipeRepository;
    constructor(recipeRepository: Repository<Recipe>);
    createRecipe(createRecipeInput: CreateRecipeInput): Promise<any>;
    delete(id: string): Promise<import("typeorm").DeleteResult>;
    getRecipe(id: string): Promise<Recipe>;
    getAllRecipes(): Promise<Recipe[]>;
    addLikeToRecipe(id: string, idRecipe: string): Promise<any>;
}
