"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileResolver = void 0;
const file_service_1 = require("./file.service");
const graphql_1 = require("@nestjs/graphql");
const apollo_server_express_1 = require("apollo-server-express");
const file_type_1 = require("./file.type");
let FileResolver = class FileResolver {
    constructor(fileService) {
        this.fileService = fileService;
    }
    file(id) {
        return this.fileService.getFile(id);
    }
    async uploadFile(file) {
        const result = await this.fileService.createFile(file);
        return result.id;
    }
};
__decorate([
    graphql_1.Query(returns => String),
    __param(0, graphql_1.Args('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], FileResolver.prototype, "file", null);
__decorate([
    graphql_1.Mutation(() => String),
    __param(0, graphql_1.Args({ name: 'file', type: () => apollo_server_express_1.GraphQLUpload })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Buffer]),
    __metadata("design:returntype", Promise)
], FileResolver.prototype, "uploadFile", null);
FileResolver = __decorate([
    graphql_1.Resolver(of => file_type_1.FileType),
    __metadata("design:paramtypes", [file_service_1.FileService])
], FileResolver);
exports.FileResolver = FileResolver;
//# sourceMappingURL=file.resolver.js.map