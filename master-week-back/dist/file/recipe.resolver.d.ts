/// <reference types="node" />
import { User } from './../user/user.entity';
import { RecipeService } from './recipe.service';
import { FileUpload } from "graphql-upload";
import { Stream } from "stream";
export interface Upload {
    filename: string;
    mimetype: string;
    encoding: string;
    createReadStream: () => Stream;
}
export declare class RecipeResolver {
    private recipeService;
    constructor(recipeService: RecipeService);
    recipe(id: string): Promise<any>;
    recipes(): Promise<any[]>;
    createRecipe(image: string, title: string, description: string, dateCreation: Date, usersId: string): Promise<any>;
    deleteRecipe(id: string): Promise<import("typeorm").DeleteResult>;
    addLikeToRecipe(userToken: User, idRecipe: string): Promise<any>;
    uploadFile({ createReadStream, filename }: FileUpload): Promise<boolean>;
}
