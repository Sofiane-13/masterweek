"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecipeService = void 0;
const recipe_entity_1 = require("./recipe.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const uuid_1 = require("uuid");
let RecipeService = class RecipeService {
    constructor(recipeRepository) {
        this.recipeRepository = recipeRepository;
    }
    createRecipe(createRecipeInput) {
        const { image, title, description, dateCreation, usersId } = createRecipeInput;
        const recipe = this.recipeRepository.create({
            id: uuid_1.v4(),
            image,
            title,
            description,
            dateCreation,
            usersIdsLiked: [],
            usersId
        });
        return this.recipeRepository.save(recipe);
    }
    delete(id) {
        return this.recipeRepository.delete({ id });
    }
    getRecipe(id) {
        return this.recipeRepository.findOne({ id });
    }
    getAllRecipes() {
        return this.recipeRepository.find();
    }
    async addLikeToRecipe(id, idRecipe) {
        const property = await this.recipeRepository.findOne({ id: idRecipe });
        console.log(property);
        return this.recipeRepository.save(Object.assign(Object.assign({}, property), { usersIdsLiked: [...property.usersIdsLiked, id] }));
    }
};
RecipeService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(recipe_entity_1.Recipe)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], RecipeService);
exports.RecipeService = RecipeService;
//# sourceMappingURL=recipe.service.js.map