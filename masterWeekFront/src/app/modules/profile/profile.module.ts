import { CameraModule } from './../../../components/camera/camera.module';
import { SharedModule } from './../../../shared/shared.module';
import { LoaderModule } from './../../../components/loader/loader.module';
import { ProfileComponent } from './coponents/profile/profile.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
  },
  {
    path: '**',
    redirectTo: 'profile',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    ProfileComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LoaderModule,
    SharedModule,
    CameraModule,
  ]
})
export class ProfileModule { }
