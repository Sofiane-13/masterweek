import { LoaderModule } from './../../../components/loader/loader.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './components/account/account.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: AccountComponent,
  },
  {
    path: '**',
    redirectTo: 'account',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AccountComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    LoaderModule,
  ]
})
export class AccountModule { }
