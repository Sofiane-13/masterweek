import { LoaderModule } from './../../../components/loader/loader.module';
import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BestRecipesComponent } from './components/best-recipes/best-recipes.component';
import { RouterModule, Routes } from '@angular/router';
import {MatCardModule} from '@angular/material/card';


const routes: Routes = [
  {
    path: '',
    component: BestRecipesComponent,
  },
  {
    path: '**',
    redirectTo: 'recipes',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    BestRecipesComponent
  ],
  imports: [
    MatCardModule,
    LoaderModule,
    MatButtonModule,
    RouterModule.forChild(routes),
    CommonModule,
  ],

  providers: []
})
export class BestModule { }
