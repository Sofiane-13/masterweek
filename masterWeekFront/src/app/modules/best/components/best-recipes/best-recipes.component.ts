import { Reciep } from './../../../../../types/recieps';
import { Apollo, gql } from 'apollo-angular';
import { Subscription } from 'rxjs';

import { Component, OnInit, OnDestroy } from '@angular/core';
@Component({
  selector: 'app-best-recipes',
  templateUrl: './best-recipes.component.html',
  styleUrls: ['./best-recipes.component.scss']
})
export class BestRecipesComponent implements OnInit, OnDestroy  {
loading = true;
recipesOfTheWeek: Reciep[];

private queryReciepSubscription: Subscription;
private queryFileSubscription: Subscription;

  constructor(private apollo: Apollo) { }

  ngOnInit(): void {
    this.queryReciepSubscription = this.apollo.watchQuery<any>({
      query: gql`
      query GetPosts {
        recipesFoTheWeek(skip: 0 take: 10) {dateCreation id likes description title images}
      }
    `
    })
      .valueChanges
      .subscribe(({ data, loading }) => {
        this.loading = loading;
        this.recipesOfTheWeek = data.recipesFoTheWeek;
        console.log('recipesOfTheWeek', this.recipesOfTheWeek)
      });
  }

public  getFile(images: string[]) {
    const firstImage = images[0];
    console.log('firstImage', firstImage);

    if (!firstImage) { return ''; }
    this.queryFileSubscription = this.apollo.watchQuery<any>({
      query: gql`
      query GetFile {
        file(id: ${firstImage})
      }
    `
    })
      .valueChanges
      .subscribe(({ data, loading }) => {
        this.loading = loading;
        console.log('data', data);
      });
  }

  ngOnDestroy(): any {
    this.queryReciepSubscription.unsubscribe();
    this.queryFileSubscription.unsubscribe();
  }

  }



