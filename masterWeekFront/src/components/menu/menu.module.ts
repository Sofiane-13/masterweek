import { SharedModule } from './../../shared/shared.module';
import { MenuComponent } from './menu.component';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    MenuComponent
  ],
  imports: [
    MatIconModule,
    SharedModule
  ],
  providers: [],
  exports: [MenuComponent]
})
export class MenuModule { }
