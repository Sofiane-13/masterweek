import { CommonModule } from '@angular/common';
import { CameraComponent } from './camera.component';
import { NgModule } from '@angular/core';
import { WebcamModule } from 'ngx-webcam';


@NgModule({
  declarations: [
    CameraComponent
  ],
  imports: [
    WebcamModule,
    CommonModule
  ],
  providers: [],
  exports: [CameraComponent]
})
export class CameraModule { }
