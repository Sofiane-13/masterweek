import { SharedModule } from './../../shared/shared.module';
import { LoaderComponent } from './loader.component';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    LoaderComponent
  ],
  imports: [
    MatIconModule,
    SharedModule
  ],
  providers: [],
  exports: [LoaderComponent]
})
export class LoaderModule { }
