import { Component } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {
  public rndInt: number;
  constructor() {
    this.rndInt = Math.floor(Math.random() * 10) + 1;
  }
}
