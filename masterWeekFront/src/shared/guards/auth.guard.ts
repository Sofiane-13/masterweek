import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { take, map, mergeMap, mapTo, filter, tap } from 'rxjs/operators';



@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {



  constructor(
    private translate: TranslateService,
    private router: Router,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return of(true);
    // return this.isAuthorized$.pipe(
    //   take(1),
    //   map((authorized: boolean) => !!authorized),
    //   mergeMap((authorized: boolean) => {
    //     if (authorized) {
    //       return this.user$.pipe(
    //         filter((user: User) => !!user),
    //         take(1),
    //         mapTo(true),
    //       );
    //     }
    //     return of(false);
    //   }),
    //   mergeMap((authorized: boolean) => {
    //     if (!authorized && state && !state.url.endsWith('/auth/login')) {
    //       return this.translate.get('SHOPM.ERRORS_MESSAGE.UNAUTHORIZED').pipe(
    //         tap((message: string) => {
    //           const toast: Toast = {
    //             message,
    //             type: ToastType.Info
    //           };
    //           this.toastService.add(toast);
    //           setTimeout(() => this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url } }), 1000);
    //         }),
    //         mapTo(authorized)
    //       );
    //     }
    //     return of(authorized);
    //   }),
    // );
  }
}
