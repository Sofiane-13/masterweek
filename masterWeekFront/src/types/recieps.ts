export type QueryRecieps = {
  recieps: Reciep[];
};

export type Reciep = {
  id: number;
  images: string[];
  title: string;
  description: string;
  dateCreation: Date;
  usersIdsLiked: string[];
  likes: number;
  usersId: string;
};
